import threading, time, os, math, xlsxwriter, sys, ctypes, glob
from jira import JIRA

class JIRA_Reading:

    def __init__(self):

        print(sys.argv[1])
        print(sys.argv[2])
        print(sys.argv[3])
        print(sys.argv[4])
        print(sys.argv[5])

        self.SEARCH_VALUE_FILTER = sys.argv[3]
        self.FILE_LOCATION = sys.argv[1]+"\Jira_Report.xlsx"
        self.MAX_RECORD_PULL = 100
        self.MAX_THREAD_COUNT = 20

        self.EXCEL_FILE = xlsxwriter.Workbook(self.FILE_LOCATION)
        self.SHEET = self.EXCEL_FILE.add_worksheet("JIRA")

        print("Establishing connecting with Jira using jksomas API Token...")

        self.JIRA = JIRA(options={"server": sys.argv[2]},
                         basic_auth=(sys.argv[4], sys.argv[5]))

        print("Fetching all custom field information...")
        ALL_FIELDS_INFO = self.JIRA.fields()

        self.COLUMN_MAPPING = {field['id']: field['name'] for field in ALL_FIELDS_INFO}
        self.COLUMN_REQUIRED = ['Issue Type', 'Key', 'Summary', 'Components', 'Assignee', 'Reporter', 'Priority', 'Status',
                          'Resolution', 'Created', 'Updated', 'Due date', 'Team', 'Sprint', 'Labels']
        try:
            ISSUES_IN_PROJ = self.JIRA.search_issues(self.SEARCH_VALUE_FILTER, json_result=True)
        except:
            ctypes.windll.user32.MessageBoxW(0, "Filter criteria error, may have single quote","Message", 1)

        self.TOTAL_ROW_COUNT = ISSUES_IN_PROJ['total']
        print ("Total no. of issues: ", self.TOTAL_ROW_COUNT)

        [self.SHEET.write(0, col, value) for col, value in enumerate(self.COLUMN_REQUIRED)]

        if (math.ceil(self.TOTAL_ROW_COUNT / self.MAX_RECORD_PULL)) <  self.MAX_THREAD_COUNT:
            self.MAX_THREAD_COUNT = math.ceil(self.TOTAL_ROW_COUNT / self.MAX_RECORD_PULL)

        self.SPLIT_COUNT = [tRange for tRange in range(0, int(math.ceil(self.TOTAL_ROW_COUNT / self.MAX_RECORD_PULL)+1), self.MAX_THREAD_COUNT)]
        self.SPLIT_COUNT.insert(len(self.SPLIT_COUNT)-1, self.SPLIT_COUNT[-1] + 20)

        print("Batch split count: {}".format(len(self.SPLIT_COUNT)))

    def threadFile(self, searchValue):

        tmp = 0
        self.requestCount = iter(threading.Thread(target=self.checkFile, args=(searchValue, (self.MAX_RECORD_PULL * tmpRequestNo)+1)) for tmpRange in self.SPLIT_COUNT for tmpRequestNo in
                                 range(self.SPLIT_COUNT[self.SPLIT_COUNT.index(tmpRange) - 1],
                                       self.SPLIT_COUNT[self.SPLIT_COUNT.index(tmpRange)]))
        while True:
            if threading.active_count() == 1:
                req = []
                tStart = time.time()
                try:
                    for n in range(int(self.MAX_THREAD_COUNT)):
                        req.append(next(self.requestCount))
                except Exception as e:
                    pass

                [tmpProcess.start() for tmpProcess in req]
                [tmpProcess.join() for tmpProcess in req]

                try:
                    print("Batch {} issues fetched and written in {:.2f} sec(s) ".format(tmp + 1, time.time()-tStart))
                except:
                    pass

                req.clear()
                tmp += 1

            if tmp == len(self.SPLIT_COUNT):
                break
            else:
                continue

    def checkFile(self, searchValue, startAt):

        try:
            issues_in_proj = self.JIRA.search_issues(searchValue, startAt=startAt, maxResults=self.MAX_RECORD_PULL)
        except Exception as e:
            issues_in_proj = self.JIRA.search_issues(searchValue, startAt=startAt, maxResults=self.MAX_RECORD_PULL)

        noOfIssues = [tmpIssue for tmpIssue in issues_in_proj]
        tmpResult = [tmpValue for tmpValue in [[tmp.key, tmp.raw['fields']] for tmp in noOfIssues]]

        def refineValue(value):

            def refineData(data):
                try:
                    if type(data) is dict:
                        return data["name"]
                    elif type(data) is list:
                        val = str(data[0])
                        if "name=" in val:
                            return val[val.find("name=") + 5: val.find(",", val.find("name="))]
                        else:
                            return (val)
                except (Exception):
                    return None

            tempDict = {self.COLUMN_MAPPING[val1[0]]: val1[1] if type(val1[1]) is str else refineData(val1[1]) for val1 in
                        value[1].items() if self.COLUMN_MAPPING[val1[0]] in self.COLUMN_REQUIRED}

            tempDict["Key"] = value[0]

            return tempDict

        [self.SHEET.write(startAt + row, self.COLUMN_REQUIRED.index(key), val) for row, value in enumerate(tmpResult) for key, val
         in
         refineValue(value).items()]

    def main(self):

        startTime = time.time()
        self.threadFile(self.SEARCH_VALUE_FILTER)

        try:
            os.mkdir(sys.argv[1])
        except FileExistsError:
            pass

        if os.path.isfile(self.FILE_LOCATION) is True:
            os.remove(self.FILE_LOCATION)

        print ("Execution complete and report generated in {}".format(self.FILE_LOCATION))

        self.EXCEL_FILE.close()
        self.JIRA.close()
        print("Time taken to generate report: {:.2f} sec(s)".format(time.time() - startTime))
        ctypes.windll.user32.MessageBoxW(0, "Report generated successfully in {}".format(self.FILE_LOCATION), "Message", 1)

if __name__ == "__main__":
    csv = JIRA_Reading()
    csv.main()